<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id')->constrained()->cascadeOnDelete();
            $table->string('bust',3)->nullable();
            $table->string('waist',3);
            $table->string('shoulder_to_waist',3)->nullable();
            $table->string('shoulder_to_nipple',3)->nullable();
            $table->string('shoulder_to_under_bust',3)->nullable();
            $table->string('nipple_to_nipple',3)->nullable();
            $table->string('across_back',3);
            $table->string('around_arm',3);
            $table->string('dress_length',3)->nullable();
            $table->string('sleeve_length',3);
            $table->string('top_length',3);
            $table->string('chest',3)->nullable();
            $table->string('neck',3)->nullable();
            $table->string('thigh',3);
            $table->string('knee',3);
            $table->string('bar',3);
            $table->string('trousers_shorts_length',3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
