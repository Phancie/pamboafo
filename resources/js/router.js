import Vue from 'vue';
import VueRouter from 'vue-router';
import Dashboard from './views/Dashboard';
import Customers from './views/customers/Index'
import Customer from './views/customers/Show'
import Measurement from './views/customers/Measurement'
import Logout from './auth/Logout'

Vue.use(VueRouter);

export default new VueRouter({
    routes:[
        { path:'/dashboard', component: Dashboard,meta:{title: 'Dashboard'}, name:'dashboard' },

        { path:'/customers', component:Customers, meta:{title:'Customers'}, name:'customers' },

        { path:'/customers/:customer', component:Customer, meta:{title:'Customer'}, name:'customer' },

        { path:'/measurements', component:Measurement, meta:{title:'Measurements'}, name:'measurements' },

        { path:'/logout', component:Logout, meta:{title:'Logout'}, name:'logout' }

    ],
    mode: 'history'
})