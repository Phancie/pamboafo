import Vue from 'vue'
import router from './router';
import App from './components/App'
import Register from './auth/Register'
import Login from './auth/Login'
import ForgotPassword from './auth/ForgotPassword'
import vuetify from './plugins/vuetify'

require('./bootstrap');

const app = new Vue({
    router,
    vuetify,
    components:{
        App,
        Register,
        Login,
        ForgotPassword
    },
    el: '#app',
});
