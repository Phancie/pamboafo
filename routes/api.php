<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::patch('/subscription/update','SubscriptionsController@update');

Route::middleware('auth:sanctum')->group(function () {

    Route::prefix('customers')->group(function (){

        Route::get('/','CustomersController@index');

        Route::post('/store','CustomersController@store');

        Route::get('/{customer}','CustomersController@show');

        Route::patch('/{customer}','CustomersController@update');

        Route::delete('/{customer}','CustomersController@destroy');
    });

    Route::prefix('measurements')->group(function (){

        Route::get('/show/{customer}','MeasurementsController@show');

        Route::post('/store','MeasurementsController@store');
    });


});
