<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class MeasurementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->role->name == 'Service Provider';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd(request()->all());
        return [
            'customer_id'=> ['required','min:1'],
            'gender'=> ['required'],
            'bust'=>[
                Rule::requiredIf(function (){
                    return request('gender') == 'Female';
                }),
                'nullable',
                'numeric',
                'min:0.1'
            ],
            'waist'=>['required','numeric','min:0.1'],
            'shoulder_to_waist'=>[
                Rule::requiredIf(function (){
                    return request('gender') == 'Female';
                }),
                'nullable',
                'numeric',
                'min:0.1'
            ],
            'shoulder_to_nipple'=>[
                Rule::requiredIf(function (){
                    return request('gender') == 'Female';
                }),
                'nullable',
                'numeric',
                'min:0.1'
            ],
            'shoulder_to_under_bust'=>[
                Rule::requiredIf(function (){
                    return request('gender') == 'Female';
                }),
                'nullable',
                'numeric',
                'min:0.1'
            ],
            'nipple_to_nipple'=>[
                Rule::requiredIf(function (){
                    return request('gender') == 'Female';
                }),
                'nullable',
                'numeric',
                'min:0.1'
            ],
            'across_back'=>['required','numeric','min:0.1'],
            'around_arm'=>['required','numeric','min:0.01'],
            'dress_length'=>[
                Rule::requiredIf(function (){
                    return request('gender') == 'Female';
                }),
                'nullable',
                'numeric',
                'min:0.1'
            ],
            'sleeve_length'=>['required','numeric','min:0.1'],
            'top_length'=>['required','numeric','min:0.1'],
            'chest'=>[
                Rule::requiredIf(function (){
                    return request('gender') == 'Male';
                }),
                'nullable',
                'numeric',
                'min:0.1'
            ],
            'neck'=>[
                Rule::requiredIf(function (){
                    return request('gender') == 'Male';
                }),
                'nullable',
                'numeric',
                'min:0.1'
            ],
            'thigh'=> ['required','numeric','min:0.1'],
            'knee' => ['required','numeric','min:0.1'],
            'bar'=>['required','numeric','min:0.1'],
            'trousers_shorts_length'=>['required','numeric','min:0.1'],
        ];
    }

//    public function withValidator($validator){
//        $validator->after(function ($validator){
//            dd($this->validator());
//        });
//    }

}
