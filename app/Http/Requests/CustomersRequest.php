<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return auth()->user()->role->name == 'Service Provider';
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required','min:2','max:20'],
            'middle_name' => ['nullable','min:2','max:20'],
            'last_name' => ['required','min:2','max:20'],
            'gender' => ['required'],
            'phone_number' => ['required','min:10','max:15','unique:customers'],
            'alternate_phone_number' => ['nullable','min:10','max:15','unique:customers'],
            'birthday' => ['nullable','date'],
            'email' => ['nullable','email','max:50','unique:customers'],
            'location' => ['required','min:3','max:50'],
            'gps_address' => ['nullable','min:5','max:20']
        ];
    }
}
