<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\CustomersRequest;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomersController extends Controller
{

    public $paginateCount = 15;

    public function index(){
        $customers = Customer::orderBy('first_name')->paginate($this->paginateCount);
        return response()->json([
            'customers' => $customers
        ]);

    }

    public function store(CustomersRequest $request){
        $validated = $request->validated();
        auth()->user()->customers()->create($validated);
        return response()->json([
            'customers' => Customer::orderBy('first_name')->paginate($this->paginateCount)
        ],Response::HTTP_CREATED);
    }

    public function show(Customer $customer){

        return response()->json([
            'customer' => $customer
        ],Response::HTTP_OK);
    }

    public function update(){

    }

    public function destroy(Customer $customer){

        $customer->delete();
        return response()->json([
            'message' => 'delete'
        ],Response::HTTP_OK);
    }
}
