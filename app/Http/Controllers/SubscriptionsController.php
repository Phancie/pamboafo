<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionsController extends Controller
{
    public function update(){

        $validated = $this->validate(request(), [
            'subscription' => ['required','numeric'],
            'email' => ['required','email']
        ]);

        $user = User::whereEmail($validated['email'])->first();
        $user->subscription_id = $validated['subscription'];
        $user->save();
        return response()->json([
            'message' =>'success'
        ],Response::HTTP_OK);
    }
}
