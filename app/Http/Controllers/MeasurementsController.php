<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\MeasurementRequest;
use App\Measurement;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MeasurementsController extends Controller
{

    public function show(Customer $customer){

        $measurement = $customer->measurement;
        return response()->json([
           'measurement' => $measurement
        ],Response::HTTP_OK);
    }

    public function store(MeasurementRequest $request){
        $validated = $request->validated();
        $customer  = Customer::find($validated['customer_id']);
        unset($validated['gender']);
        $customer->measurement()->create($validated);
        return response()->json([
            'message' => 'created'
        ],Response::HTTP_CREATED);
    }
}
