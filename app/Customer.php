<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];

    public function setEmailAttribute($value) {
        if ( empty($value) ) {
            $this->attributes['email'] = NULL;
        } else {
            $this->attributes['email'] = $value;
        }
    }

    public function setAlternatePhoneNumberAttribute($value) {
        if ( empty($value) ) {
            $this->attributes['alternate_phone_number'] = NULL;
        } else {
            $this->attributes['alternate_phone_number'] = $value;
        }
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function measurement(){
        return $this->hasOne(Measurement::class);
    }
}
